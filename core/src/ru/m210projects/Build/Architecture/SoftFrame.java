package ru.m210projects.Build.Architecture;

public interface SoftFrame {
	
	public byte[] getFrame();
	
	public void changepalette(byte[] palette);

}
