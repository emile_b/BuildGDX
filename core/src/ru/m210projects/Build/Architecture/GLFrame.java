package ru.m210projects.Build.Architecture;

public interface GLFrame {

	public boolean setDisplayConfiguration(float gamma, float brightness, float contrast);

	public void setDefaultDisplayConfiguration();
	
}
